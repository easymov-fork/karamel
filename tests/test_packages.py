import unittest
import os, tempfile, shutil

from karamel.packages import *
from karamel.exception import PackagesFileNotFound


class TestConfig(unittest.TestCase):

    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_read_online_packages_file(self):
        packages = read_packages_file('https://gitlab.com/karamel/tests/package_manager/raw/master/packages.yml')
        self.assertEqual(len(packages), 2)

    def test_read_local_packages_file(self):
        packages_file_name = 'packages.yml'
        packages_file = \
'''package_1:
  url: https://gitlab.com/karamel/tests/package_1.git
  description: Description for package_1
package_2:
  url: https://gitlab.com/karamel/tests/package_2.git
  description: Description for package_2'''

        with open(packages_file_name, 'w') as stream:
            stream.write(packages_file)

        packages = read_packages_file(packages_file_name)
        self.assertEqual(len(packages), 2)

    def test_read_no_packages_file(self):
        packages_file_name = 'packages.yml'
        with self.assertRaises(PackagesFileNotFound):
            packages = read_packages_file(packages_file_name)
