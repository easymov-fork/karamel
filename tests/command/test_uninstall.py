import unittest, sys
import os, tempfile, shutil

import karamel.command.uninstall
from karamel.command.uninstall import *
from karamel.exception import KaramelException

class TestUninstallCommand(unittest.TestCase):

    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

        self.packages_dir = os.path.join(self.tmp, 'packages')
        packages_manager = ['https://gitlab.com/karamel/tests/package_manager/raw/master/packages.yml']
        self.command = UninstallCommand(packages_manager, self.packages_dir)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_command_run(self):
        karamel.command.uninstall.input = lambda message: 'y'
        sys.argv = ['', 'package_1']
        os.makedirs(os.path.join(self.packages_dir, 'package_1'))
        self.command.run()

    def test_command_run_arg_yes(self):
        sys.argv = ['', 'package_1', '--yes']
        os.makedirs(os.path.join(self.packages_dir, 'package_1'))
        self.command.run()

    def test_command_run_bad_response(self):
        karamel.command.uninstall.input = lambda message: 'diekoe'
        sys.argv = ['', 'package_1']
        os.makedirs(os.path.join(self.packages_dir, 'package_1'))
        with self.assertRaises(KaramelException):
            self.command.run()

    def test_command_run_package_not_find(self):
        sys.argv = ['', 'package_not_find']
        with self.assertRaises(KaramelException):
            self.command.run()
