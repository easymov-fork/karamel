import unittest, sys
import os, tempfile, shutil

from karamel.command.search import *

class TestSearchCommand(unittest.TestCase):

    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

        packages_manager = ['https://gitlab.com/karamel/tests/package_manager/raw/master/packages.yml']
        self.command = SearchCommand(packages_manager)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_command_run(self):
        sys.argv = ['', 'pattern']
        self.command.run()

    def test_command_run_with_result(self):
        sys.argv = ['', 'package']
        self.command.run()
