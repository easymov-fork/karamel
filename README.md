# Karamel

[![build status](https://gitlab.com/karamel/karamel/badges/master/build.svg)](https://gitlab.com/karamel/karamel/commits/master)
[![coverage](https://gitlab.com/karamel/karamel/badges/master/coverage.svg?job=coverage)](https://karamel.gitlab.io/karamel/coverage)
[![PyPI version](https://badge.fury.io/py/karamel.svg)](https://badge.fury.io/py/karamel)

Karamel is a library that let you build powerful Package Manager based on Git.

Need more info, look at the homepage documentation. [karamel.gitlab.io](http://karamel.gitlab.io/)

## Install

```
pip install karamel
```
